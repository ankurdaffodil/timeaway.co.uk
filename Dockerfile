# use node 7.0 base image
FROM node:7-wheezy

# set working directory and include files
WORKDIR /app
ADD . /app

RUN \
    npm install -g webpack && \
    npm install && \
    webpack

# run the stub entrypoint (needs to be overridden)
CMD [ "echo", "Hello World!" ]
