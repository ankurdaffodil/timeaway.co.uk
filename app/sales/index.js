const path        = require( "path" );
const process     = require( "process" );
const compression = require( "compression" );
const express     = require( "express" );
const bodyParser  = require( "body-parser" );
const hostCoerce  = require( "maambmb-host-coerce" );
const healthcheck = require( "maambmb-alb-healthcheck" );

const cfg         = require( "../../cfg" );
const { middleware, sendEmail } = require("../../lib");
const staticDir = path.resolve( __dirname , "../../www/sales" );

async function sendEmailForm( req, res, next ) {
    try {
        await sendEmail( {
            body: `<pre>${ JSON.stringify( req.body, null , 4) }</pre>`,
            subject: req.body.subject,
            recipients: [ cfg[ process.env.ZONE ].emailRecip ]
        } );
        res.json({}); //return
    } catch( e ) {
        next( e );
    }
}

express()
    .use( healthcheck )
    .use( hostCoerce( "www.timeaway.co.uk" ) )
    .use( compression() )
    .use("/api", 
        express()
            .use( bodyParser.json() )
            .post( "/send_email_form", sendEmailForm )
            .use( middleware.handleError )
    )
    .use( express.static( staticDir ) )
    .listen( 8080 );
