const path = require( "path" );

module.exports = {
    entry : {
        // sales pages
        "sales" : "./www/sales/index.js",
        "sales/policies" : "./www/sales/policies/index.js",
        "sales/partners" : "./www/sales/partners/index.js",
        "sales/gurus" : "./www/sales/gurus/index.js",
        "sales/create-holiday" : "./www/sales/create-holiday/index.js",
        "sales/about-us" : "./www/sales/about-us/index.js",
        "sales/super-mums" : "./www/sales/super-mums/index.js",
        "sales/photo-guidelines" : "./www/sales/photo-guidelines/index.js",
        "sales/thank-you" : "./www/sales/thank-you/index.js"
    },
    "output" : {
        "path": path.join( __dirname, "www" ),
        "filename" : "[name]/index.bundle.js"
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: "babel-loader",
                    options: {
                        "presets" : [ "es2015", "react" ]
                    }
                }
            },
            {
                test: /\.css$/,
                use: [ "style-loader", "css-loader" ]
            },
            { 
                test: /\.(png|woff|woff2|eot|ttf|svg)$/, 
                loader: "url-loader?limit=100000" 
            }
        ]
    }
};
