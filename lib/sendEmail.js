const AWS = require("aws-sdk");

const sesClient = new AWS.SES(); // create AWS Simple Email Service

// Converts AWS SDK email send method to Promise
// For further info, see: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/SES.html#sendEmail-property
module.exports = function ( opts ) {
    console.log( opts );
    const parms = {
        Destination: { ToAddresses: opts.recipients },
        Message: {
            Body: {
                Html: {
                    Charset: "UTF-8", 
                    Data: opts.body // is html
                }
            },
            Subject: {
                Charset: "UTF-8", 
                Data: opts.subject
            }
        },
        Source: "mailer@mailer.timeaway.co.uk",
    };
    return new Promise( function( resolve, reject ){
        sesClient.sendEmail( parms, ( err, data ) => err ? reject( err ) : resolve( data )); // resolve returns message id e.g. "EXAMPLE78603177f-7a5433e7-8edb-42ae-af10-f0181f34d6ee-000000"
    });    
};
