const femto = require( "femto-log" )();

function handleError( error, _req, res, _next ) {
    var code = error.code || -1;
    var msg = error.toString();
    femto[ code > -1 ? "warn" : "error" ]( msg );
    res.json( { error : { msg, code } } );
}

module.exports = { handleError };
