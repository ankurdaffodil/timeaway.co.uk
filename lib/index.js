const middleware = require("./middleware");
const sendEmail  = require("./sendEmail");
    
module.exports = {
    middleware,
    sendEmail
};
