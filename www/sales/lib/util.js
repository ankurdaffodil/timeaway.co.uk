function unwrap( response ) {
    if( response.data.error )
        return Promise.reject( response.data.error );
    return Promise.resolve( response.data.result || null );
}

module.exports = {
    unwrap
};