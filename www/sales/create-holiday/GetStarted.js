import React from "react";
import Radium from "radium";
import _ from "lodash";
import TaButton from "../../common/components/TaButton";
import typography from "../constants/typography";
import colors from "../constants/colors";

function GetStarted( props ){
    const styles = {
        row: {
            minHeight:"",
            height: "100vh",
            padding: "20px",
            display: "flex",
            alignItems: "center"
        },
        mainCard: {
            width: "100%",
            maxWidth: "400px",
            padding: "20px",
            position: "relative",
            margin: "0 auto",
            backgroundImage: "url('./img/clouds.png')",
            backgroundColor: "white",
            backgroundSize: "cover cover",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "right bottom",
            logo: {
                width: "100%",
                maxWidth: "270px"
            }
        },
        heading: _.extend( {}, typography.h2.playfair, {
            margin: "10px 0px",
            color: colors.brandLight
        }),
        blurb: _.extend( {}, typography.p.montserrat, {
            margin: "10px 0px 20px",
            color: colors.textDefault,
            fontWeight: "300"
        }),
        transparent: {
            background: "rgba(255,255,255,0.6)"
        }
    };
    return(
        <div className="grad-blue-skies" style={ styles.row }>
            <div className="md-card" style={ styles.mainCard }>
                <center style={{padding: "0px 0px 10px"}}>
                    <img src="../img/logo.png" style={ styles.mainCard.logo }/>
                    <div style={ styles.heading }>It&prime;s time to Make Memories.</div>
                    <div style={ styles.blurb }>
                        Tell us a bit about what your family likes,
                        and we will send you a few suggestions for beautiful, 
                        quality family TimeAway.
                    </div>
                    <input 
                        className="form-control" 
                        style={ styles.transparent } 
                        placeholder="Name" 
                        onChange={ ( e ) => props.handleInputChange( "name", e ) }
                        value={ props.data.name } />
                    <input 
                        className="form-control" 
                        style={ styles.transparent } 
                        placeholder="Email" 
                        onChange={ ( e ) => props.handleInputChange( "email", e )}
                        value={ props.data.email } />
                    <input 
                        className="form-control" 
                        style={ styles.transparent } 
                        placeholder="Phone Number" 
                        onChange={ ( e ) => props.handleInputChange("mobileNumber", e ) }
                        value={ props.data.mobileNumber } />
                    
                    <TaButton onClick={ () => props.toggleShowGetStarted() }>
                        Get Started
                    </TaButton>
                </center>
            </div>
        </div>
    );
}

module.exports = Radium( GetStarted );