import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import ReactDOM from "react-dom";
import { StyleRoot } from "radium";
import FixedNav from "../components/FixedNav";
import GetStarted from "./GetStarted"; // takes first few details
import MainFormBody from "./MainFormBody"; // takes remaining details
import Footer from "../components/Footer";

class App extends React.Component{
    constructor(){
        super();
        this.state = {
            // collected in GetStarted component
            name : "",
            email : "",
            mobileNumber : "",

            // collected in MainFormBody component
            family : [],
            desiredDestination : "",
            desiredTime : "",
            desiredDuration : "",
            budget : "",
            activityTypes : [],
            accommodationTypes: [],
            optionalMessage: "",

            // flags
            _showGetStarted: true
        };
    }
    toggleShowGetStarted(){
        this.setState({ _showGetStarted: !this.state._showGetStarted });
    }
    handleInputChange( stateKey , e ) {
        let stateObj = {};
        stateObj[ stateKey ] = e.target.value;
        this.setState( stateObj );
    }
    handleDropdownChange( stateKey, e ) {
        let obj = {};
        obj[ stateKey ] = e.target.value;
        this.setState( obj );
    } 
    handleCheckboxChange( stateKey, e ) {
        let value = e.target.value;
        let checked = e.target.checked;
        if( checked ) {
            // push to duplicate array
            let arr = this.state[ stateKey ].slice();
            arr.push( value );
            
            // create obj, then setstate
            let obj = {};
            obj[ stateKey ] = arr;
            this.setState( obj );
        } else {
            let index = this.state[ stateKey ].indexOf( value );
            let arr = this.state[ stateKey ].slice();
            arr.splice( index, 1);

            let obj = {};
            obj[ stateKey ] = arr;
            this.setState( obj );
        }
    }
    handleFamilyMemberAdd( memberType ) {
        const arr = this.state.family.slice(); //duplicates array
        arr.push( memberType );
        this.setState({
            family : arr
        });
    }
    handleFamilyMemberRemove( memberType ) {
        let ix = this.state.family.indexOf( memberType );
        let arr = this.state.family.slice();
        let res = arr.slice( 0, (ix) )
            .concat( arr.slice( (ix + 1 ) ) );
        
        this.setState({
            family: res
        });
    }
    render(){
        return(
            <StyleRoot>
                <FixedNav />
                
                {
                    this.state._showGetStarted &&
                        <GetStarted 
                            toggleShowGetStarted={ this.toggleShowGetStarted.bind(this) }
                            handleInputChange={ this.handleInputChange.bind(this) }
                            data={{
                                name : this.state.name,
                                email: this.state.email,
                                mobileNumber: this.state.mobileNumber
                            }}/>
                }
                
                {
                    !this.state._showGetStarted &&
                        <div className="navbar-offset">
                            <MainFormBody 
                                toggleShowGetStarted={ this.toggleShowGetStarted.bind( this ) } 
                                handleInputChange={ this.handleInputChange.bind( this ) }
                                handleDropdownChange={ this.handleDropdownChange.bind( this ) }
                                handleCheckboxChange={ this.handleCheckboxChange.bind( this ) }
                                handleFamilyMemberAdd={ this.handleFamilyMemberAdd.bind( this ) }
                                handleFamilyMemberRemove={ this.handleFamilyMemberRemove.bind(this) }
                                data={ this.state } />
                        </div>       
                }
                <Footer />
            </StyleRoot>
        );    
    }
}

document.addEventListener("DOMContentLoaded", function(){
    ReactDOM.render(
        <App />,
        document.getElementById("root")
    );
}); 