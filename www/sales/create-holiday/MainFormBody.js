import React from "react";
import Radium from "radium";
import axios from "axios";
import { unwrap } from "../lib/util";
import _ from "lodash";
import moment from "moment";
import typography from "../constants/typography";
import colors from "../constants/colors";
import { Grid, Row, Col, Checkbox } from "react-bootstrap";
import TaButton from "../../common/components/TaButton";
import { TiUserAdd } from "react-icons/lib/ti/";

class CreateHolidayBody extends React.Component {
    handleSubmit( e ) {
        e.preventDefault();
        axios({
            url: "/api/send_email_form",
            method: "POST",
            data: {
                subject: "Create Holiday Request",
                name: this.props.data.name,
                email: this.props.data.email,
                mobileNumber: this.props.data.mobileNumber,
                family: this.props.data.family.toString(),
                desiredDestination: this.props.data.desiredDestination,
                desiredTime: this.props.data.desiredTime,
                budget: this.props.data.budget,
                activityTypes: this.props.data.activityTypes.toString(),
                accommodationTypes: this.props.data.accommodationTypes.toString(),
                optionalMessage: this.props.data.optionalMessage
            }
        })
        .then( unwrap )
        .then( window.location = "/thank-you");
    }

    render(){
        const styles = {
            heading: _.extend({}, typography.h1.playfair, {

            }),
            subHeading: _.extend({}, typography.misc.capsLabel, {
                marginTop: "10px",
                marginBottom: "6px",
                color: colors.brandDark
            }),
            subSubHeading: {
                fontSize: "10px",
                color: colors.brandMediumLight,
                letterSpacing:"1px"
            },
            checkboxes: {
                fontSize: "12px"
            },
            textarea: {
                resize: "none",
                height: "112px"
            }, 
            mobOnlyFamilyBox: {
                padding: "20px 0px",
                "@media screen and (min-width: 991px)" : {
                    display: "none"
                } 
            },
            ticket: {
                width: "100%",
                height: "auto",
                minHeight: "450px",
                overflow: "auto",
                background: "#DFEFFF",
                marginTop: "0px",
                fontFamily: "Courier New",
                fontSize: "12px",
                padding: "5px 20px 20px",
                borderTop: "solid 5px " + colors.brandLight, 
                heading: {
                    fontWeight: "900",
                    fontSize: "20px",
                    marginBottom: "10px",
                    color: colors.brandLight
                },
                well: {
                    border: "solid 2px " + colors.brandSuperLight,
                    padding: "10px 15px",
                    borderRadius: "12px",
                    background: "#EFF5FB",
                    marginBottom: "10px"
                },
                wellHeading: {
                    fontSize: "14px",
                    fontWeight: "900",
                    paddingBottom: "5px",
                    marginBottom: "8px",
                    borderBottom: "solid 1px rgba(0,0,0,0.1)",
                    color: colors.brandDark
                },
                wellUl: {
                    padding: "0px 10px"
                },
                familyImg: {
                    width: "60px"
                },
                noFamilyMembersBox: {
                    width: "100%",
                    height: "84.2px",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center"
                },
                destinationImg: {
                    width: "100%",
                    maxWidth: "300px"
                },
                "@media screen and (max-width:991px)" : {
                    display: "none"
                }
            }
        };

        const dates = _.map( _.range(20),  function(_x, ix) {
            return {
                key: moment().add( ix, "months" ).format("MMM YYYY"), 
                prettyText: moment().add( ix, "months" ).format("MMM YYYY")
            };
        } );
        return(
            <Grid>
                <br/>
                <Row>
                    <Col md={6}>
                        <Col sm={6} className="col-lowpad">
                            <TaButton 
                                keepCase={true} 
                                margin="0px" 
                                padding="7px" 
                                btnType="dark"
                                onClick={ () => this.props.handleFamilyMemberAdd( "dad" ) }>
                                <TiUserAdd /> Add Dad
                            </TaButton>
                        </Col>
                        <Col sm={6} className="col-lowpad">
                            <TaButton 
                                keepCase={true} 
                                margin="0px" 
                                padding="7px" 
                                btnType="default"
                                onClick={ () => this.props.handleFamilyMemberAdd( "mum" ) }>
                                <TiUserAdd /> Add Mum
                            </TaButton>
                        </Col>
                        <Col sm={6} className="col-lowpad">
                            <TaButton 
                                keepCase={true} 
                                margin="0px" 
                                padding="7px" 
                                btnType="light"
                                onClick={ () => this.props.handleFamilyMemberAdd( "child" ) }>
                                <TiUserAdd /> Add Child
                            </TaButton>
                        </Col>
                        <Col sm={6} className="col-lowpad">
                            <TaButton 
                                keepCase={true} 
                                margin="0px" 
                                padding="7px" 
                                btnType="superLight"
                                onClick={ () => this.props.handleFamilyMemberAdd( "baby" ) }>
                                <TiUserAdd /> Add Baby
                            </TaButton>
                        </Col>
                        <Col sm={12}>
                            <div style={ styles.mobOnlyFamilyBox }>
                                {
                                    this.props.data.family.length > 0 ?
                                        this.props.data.family.map(( familyKey, ix ) => 
                                                <a  onClick={ () => this.props.handleFamilyMemberRemove( familyKey ) }
                                                    key={ ix }>
                                                    <img 
                                                        style={ styles.ticket.familyImg } 
                                                        src={ `./img/${familyKey}.png` } />
                                                </a> ) :
                                            <div style={ styles.ticket.noFamilyMembersBox }>
                                                None added yet!
                                            </div>
                                }
                            </div>
                        </Col>
                        <Col sm={12} className="col-lowpad">
                            <Row>
                                <Col sm={6}>
                                    <div style={ styles.subHeading }>Where To?</div>
                                    <input 
                                        className="form-control" 
                                        placeholder="e.g. Cornwall"
                                        onChange={ ( e ) => this.props.handleInputChange( "desiredDestination", e ) }/>
                                </Col>
                                <Col sm={6}>
                                    <div style={ styles.subHeading }>What Month?</div>
                                    <select 
                                        className="form-control" 
                                        onChange={ ( e ) => this.props.handleDropdownChange( "desiredTime", e ) }>
                                        {
                                            dates.map( ( obj, ix ) => 
                                                <option key={ ix } value={ obj.key }>
                                                    { obj.prettyText }
                                                </option> 
                                            )
                                        }
                                    </select>
                                </Col>
                                <Col sm={6}>
                                    <div style={ styles.subHeading }>For How Long?</div>
                                    <select 
                                        className="form-control"
                                        onChange={ ( e ) => this.props.handleDropdownChange( "desiredDuration", e ) }>
                                        <option value="2 days">2 days</option>
                                        <option value="3 days">3 days</option>
                                        <option value="4 days">4 days</option>
                                        <option value="5 days">5 days</option>
                                        <option value="6 days">6 days</option>
                                        <option value="7 days">1 Week</option>
                                        <option value="2 weeks">2 Weeks</option>
                                        <option value="3 weeks">3 Weeks</option>
                                    </select>
                                </Col>
                                <Col sm={6}>
                                    <div style={ styles.subHeading }>Our Budget</div>
                                    <select 
                                        className="form-control"
                                        onChange={ ( e ) => this.props.handleDropdownChange( "budget", e ) }>
                                        <option value="&pound;500">&pound;500</option>
                                        <option value="&pound;1000">&pound;1000</option>
                                        <option value="&pound;1500">&pound;1500</option>
                                        <option value="&pound;2000">&pound;2000</option>
                                        <option value="&pound;2500">&pound;2500</option>
                                        <option value="&pound;3000">&pound;3000</option>
                                        <option value="&pound;3500">&pound;3500</option>
                                        <option value="&pound;4000">&pound;4000</option>
                                        <option value="&pound;4500">&pound;4500</option>
                                        <option value="&pound;5000">&pound;5000</option>
                                        <option value="&pound;6000">&pound;6000</option>
                                        <option value="&pound;7000">&pound;7000</option>
                                        <option value="&pound;8000">&pound;8000</option>
                                        <option value="&pound;9000">&pound;9000</option>
                                        <option value="&pound;10,000+">&pound;10,000+</option>
                                    </select>
                                </Col>
                            </Row>
                            <Row>
                                <Col sm={5}>
                                    <div style={ styles.subHeading }>Activity Type</div>
                                    <div style={ styles.subSubHeading }>Multiple Allowed</div>
                                    <div style={ styles.checkboxes }>
                                        <Checkbox 
                                            value="Charm &amp; Luxury" 
                                            onChange={ ( e ) => this.props.handleCheckboxChange( "activityTypes", e ) }>
                                            Charm &amp; Luxury
                                        </Checkbox>
                                        <Checkbox 
                                            value="Off the beaten track"
                                            onChange={ ( e ) => this.props.handleCheckboxChange( "activityTypes", e ) }>
                                            Off the Beaten Track
                                        </Checkbox>
                                        <Checkbox 
                                            value="Classics"
                                            onChange={ ( e ) => this.props.handleCheckboxChange( "activityTypes", e ) }>
                                            Classics
                                        </Checkbox>
                                        <Checkbox 
                                            value="Sport &amp; Adventure"
                                            onChange={ ( e ) => this.props.handleCheckboxChange( "activityTypes", e ) }>
                                            Sport &amp; Adventure
                                        </Checkbox>
                                    </div>
                                </Col>
                                <Col sm={7}>
                                    <div style={ styles.subHeading }>Accommodation</div>
                                    <div style={ styles.subSubHeading }>Multiple Allowed</div>
                                    <div style={ styles.checkboxes }>
                                        <Checkbox 
                                            value="Authentic (homestay / guesthouse)"
                                            onChange={ ( e ) => this.props.handleCheckboxChange( "accommodationTypes", e ) }>
                                            Authentic (homestay / guesthouse)
                                        </Checkbox>
                                        <Checkbox 
                                            value="Simple (equivalent of 2* hotels)"
                                            onChange={ ( e ) => this.props.handleCheckboxChange( "accommodationTypes", e ) }>
                                            Simple (equivalent of 2* hotels)
                                        </Checkbox>
                                        <Checkbox 
                                            value="Comfortable (equivalent of 3* hotels)"
                                            onChange={ ( e ) => this.props.handleCheckboxChange( "accommodationTypes", e ) }>
                                            Comfortable (equivalent of 3* hotels)
                                        </Checkbox>
                                        <Checkbox 
                                            value="Luxury (equivalent of 4/5* hotels)"
                                            onChange={ ( e ) => this.props.handleCheckboxChange( "accommodationTypes", e ) }>
                                            Luxury (equivalent of 4* &amp; 5* hotels)
                                        </Checkbox>
                                        <Checkbox 
                                            value="Quirky (something completely different)"
                                            onChange={ ( e ) => this.props.handleCheckboxChange( "accommodationTypes", e ) }>
                                            Quirky (something completely different)
                                        </Checkbox>
                                    </div>
                                </Col>
                                <Col xs={12}>
                                    <textarea 
                                        className="form-control" 
                                        style={ styles.textarea }
                                        placeholder="Add Message"
                                        onChange={ ( e ) => this.props.handleInputChange( "optionalMessage", e ) } >
                                    </textarea>

                                    <TaButton btnType="superLight" onClick={ () => this.props.toggleShowGetStarted() }>
                                        Go Back
                                    </TaButton>
                                    <TaButton onClick={ ( e ) => this.handleSubmit( e ) }>
                                        Let&prime;s Make Memories
                                    </TaButton>
                                </Col>
                            </Row>
                        </Col>
                    </Col>
                    <Col md={6}>
                        <div style={ styles.ticket } className="ticket">
                            <div style={ styles.ticket.heading }>
                                <img src="../img/logo.png" width="180px" />
                            </div>
                            <div style={ styles.ticket.well }>
                                <div style={ styles.ticket.wellHeading }>TimeAway Leader</div>
                                <table style={{ fontSize: "12px", width: "100%" }}>
                                    <thead></thead>
                                    <tbody>
                                        <tr>   
                                            <th>
                                                Name:
                                            </th>
                                            <td style={{textAlign: "right"}}>
                                                { this.props.data.name }
                                            </td>
                                        </tr>
                                        <tr>   
                                            <th>
                                                Email:
                                            </th>
                                            <td style={{textAlign: "right"}}>
                                                { this.props.data.email }
                                            </td>
                                        </tr>
                                        <tr>   
                                            <th>
                                                Mobile:
                                            </th>
                                            <td style={{textAlign: "right"}}>
                                                { this.props.data.mobileNumber }
                                            </td>
                                        </tr>
                                        <tr>   
                                            
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div style={ styles.ticket.well }>
                                <div style={ styles.ticket.wellHeading }>Family</div>
                                {
                                    this.props.data.family.length > 0 ?
                                        this.props.data.family.map(( familyKey, ix ) => 
                                                <a  onClick={ () => this.props.handleFamilyMemberRemove( familyKey ) }
                                                    key={ ix }>
                                                    <img 
                                                        style={ styles.ticket.familyImg } 
                                                        src={ `./img/${familyKey}.png` } />
                                                </a> ) :
                                        <div style={ styles.ticket.noFamilyMembersBox }>
                                            None added yet!
                                        </div>
                                }
                            </div>
                            <div style={ styles.ticket.well }>
                                <div style={ styles.ticket.wellHeading }>Details</div>
                                <Row>
                                    <Col sm={4}>
                                        <b>Destination: </b>
                                        <div style={{ wordBreak: "break-all", hyphens: "auto", marginBottom: "5px" }}>
                                            { 
                                                this.props.data.desiredDestination ? 
                                                    this.props.data.desiredDestination :
                                                    "None selected"
                                            }
                                        </div>

                                        <b>During: </b>
                                        <div style={ {marginBottom: "5px"} }>
                                            {/* TODO: save desired time as (unix timestamp OR IsoFormat)  */}
                                            { this.props.data.desiredTime ? moment(this.props.data.desiredTime).format("MMM YYYY") : "None Selected" }
                                        </div>

                                        <b>Duration: </b>
                                        <div style={ {marginBottom: "5px"} }>
                                            { this.props.data.desiredDuration ? this.props.data.desiredDuration : "None Selected" }
                                        </div>

                                        <b>Our Budget: </b>
                                        <div style={ {marginBottom: "5px"} }>
                                            { this.props.data.budget ? this.props.data.budget : "None Selected" }
                                        </div>
                                        
                                    </Col>
                                    <Col sm={8}>
                                        <b>Activity Type:</b>
                                        <ul style={ styles.ticket.wellUl }>
                                            { 
                                                this.props.data.activityTypes.length > 0 ? 
                                                    this.props.data.activityTypes.map(( activity, ix ) => <li key={ix}>{ activity }</li> ) :
                                                    <li>None Selected</li>
                                            }
                                        </ul>

                                        <b>Accomodation:</b>
                                        <ul style={ styles.ticket.wellUl }>
                                            {
                                                this.props.data.accommodationTypes.length > 0 ? 
                                                    this.props.data.accommodationTypes.map(( accommodation, ix ) => <li key={ix}>{ accommodation }</li>) :
                                                    <li>None Selected</li>
                                            }
                                        </ul>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                    </Col>

                </Row>
            </Grid>
        );
    }
}

module.exports = Radium( CreateHolidayBody );