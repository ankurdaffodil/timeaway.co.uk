import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import ReactDOM from "react-dom";
import { StyleRoot } from "radium";
import FixedNav from "../components/FixedNav";
import ShortBanner from "../components/ShortBanner";
import PhotoGuidelineBody from "./PhotoGuidelineBody";
import Footer from "../components/Footer";

function App(){
    return(
        <StyleRoot>
            <FixedNav />
            <div className="navbar-offset">
                <ShortBanner 
                    imgSrc="../img/team.png"
                    heading="Dress to Impress."
                    subHeading="Our mission is to help families spend quality time together - making memories that last a lifetime. We are TimeAway." 
                    btnText="Become a Guru"
                    btnHref="/gurus"/>

                <PhotoGuidelineBody />

                <Footer />
            </div>
        </StyleRoot>
    );
}

document.addEventListener("DOMContentLoaded", function(){
    ReactDOM.render(
        <App />,
        document.getElementById("root")
    );
});