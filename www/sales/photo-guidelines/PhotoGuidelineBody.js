import React from "react";
import Radium from "radium";
import _ from "lodash";
import typography from "../constants/typography";
import colors from "../constants/colors";
import { Grid, Row, Col } from "react-bootstrap";

function PhotoGuidelineBody (){
    const styles = {
        heading : _.extend({}, typography.h1.playfair, {
            marginTop: "50px"
        }),
        subHeading : _.extend({}, typography.h2.montserrat, {
            marginBottom: "15px"
        }),
        paragraph : _.extend({}, typography.p.montserrat, {
            color: colors.textLight,
            maxWidth: "500px"
        }),
        listItem: _.extend({}, typography.h3.montserrat, {
            color: colors.textDefault,
            marginBottom: "10px"
        })
    };
    return(
        <Grid>
            <Row>
                <Col xs={12}>
                    <div style={ styles.heading }>Photo Guidelines</div>
                    <div className="separator"></div>
                </Col>
                <Col xs={12}>
                    <div style={ styles.subHeading }>
                        It&prime;s time to have your picture taken.
                    </div>
                    <div style={ styles.paragraph }>
                        Once your done, families will be able to see the character in the experiences you provide.
                        We are so excited to have you on-board.
                    </div>
                    <ol style={{padding:"25px"}}>
                        <li style={ styles.listItem }>Switch on the lights if indoors, and shoot at a time with good natural light</li>
                        <li style={ styles.listItem }>No fish-eye lenses, panoramas or filters</li>
                        <li style={ styles.listItem }>No people or pets in shot </li>
                        <li style={ styles.listItem }>Shoot horizontally</li>
                        <li style={ styles.listItem }>Include details - a nice piece of art, furniture or decoration</li>
                        <li style={ styles.listItem }>If you are shooting indoor accomodation, include at least one for each room - including bathrooms</li>
                        <li style={ styles.listItem }>Send us the largest file possible</li>
                    </ol>
                    <br/>
                    <br/>
                </Col>
            </Row>
        </Grid>
    );
}

module.exports = Radium( PhotoGuidelineBody );