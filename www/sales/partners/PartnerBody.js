import React from "react";
import Radium from "radium";
import _ from "lodash";
import { Grid, Row, Col } from "react-bootstrap";
import colors from "../constants/colors";
import typography from "../constants/typography";
import TaButton from "../../common/components/TaButton";
import axios from "axios";
import { unwrap } from "../lib/util";

class AffiliateBody extends React.Component {
    constructor(){
        super();
        this.state = {
            name : "",
            phoneNumber : "",
            email : "",
            message: ""
        };
        this.handleInputChange = this.handleInputChange.bind(this);
    }
    handleInputChange( stateKey, e ){
        const stateObj = {};
        stateObj[ stateKey ] = e.target.value;
        this.setState( stateObj, () => console.log( this.state ) );

    }
    handleSubmit( e ) {
        e.preventDefault();
        axios({
            url: "/api/send_email_form",
            method: "POST",
            data: {
                subject: "TimeAway Partner Application",
                name: this.state.name,
                phoneNumber: this.state.email,
                email: this.state.phoneNumber,
                message: this.state.message
            }
        })
        .then( unwrap )
        .then( window.location = "/thank-you" );
    } 
    render() {
        const styles = {
            heading: _.extend({}, typography.h1.playfair, {
                marginTop: "30px",
                color: colors.textDefault
            }),
            subHeading: _.extend({}, typography.h3.montserrat, {
                marginBottom: "30px",
                maxWidth: "480px"
            }),
            paragraph: _.extend({}, typography.p.montserrat, {
                color: colors.textDefault
            }),
            formBox: {
                width: "100%",
                height: "auto",
                marginLeft: "auto",
                marginRight: "auto",
                maxWidth: "550px",
                background: "#EDF5FB",
                padding: "20px",
                boxShadow: "0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)",
                heading: _.extend( {}, typography.h3.montserratUppercase, {
                    color: colors.brand
                }),
                paragraph: _.extend( {}, typography.p.montserrat, {
                    color: colors.textDefault
                }),
                image: {
                    width: "100%",
                    maxWidth: "270px"
                }
            },
            textarea: { resize: "none" }
        };
        return (
            <Grid style={ styles.container }>
                <div style={ styles.heading }>
                    TimeAway Partner Program
                </div>

                <div className="separator"></div>

                <div style={ styles.subHeading }>
                    We run an family-focused travel service, saving families time and effort organising: active holidays, courses and experiences.
                </div>

                <div style={ styles.paragraph }>
                    Through us they can book direct with thousands of family travel experts; local, independent and ethical operators around the world. We work successfully with many publishers to bring this service to their readers, adding value and a strong new revenue stream. We work to understand individual partners objectives to provide customisable solutions. Our partners range from some of the UK&prime;s leading family brands, media companies and niche travel websites.
                    <br/><br/>
                    Benefits to our partners include:
                    <br/><br/>
        
                    <ul>
                        <li>Increased Revenue (Earn unlimited cash or credits every time your referrals book with TimeAway).</li>
                        <li>Improved User Experience (Seamlessly deliver your audience content relevant to their interests).</li>
                        <li>Exceptional Affiliate Support (Our marketing team is here to answer any of your enquiries).</li>
                        <li>24/7 Customer Support (most seamless trip booking platform, led by a team of travel experts who work 24/7, 365 days a year to answer customer enquiries).</li>
                        <li>Easy Implementation (Get started in minutes. No technical skills required).</li>
                        <li>No Obligations (There are no minimum commitments or contracts. Start and stop whenever you want).</li>
                    </ul>
                </div>
                <br/><br/>
                <div style={ styles.formBox }>
                    <center>
                        <img src="../img/partner-logo.png" style={ styles.formBox.image } />
                        <div className="separator"></div>
                        <div style={ styles.formBox.heading }>Find Out More</div> 
                        <br/>
                        <div style={ [ styles.formBox.paragraph, { maxWidth:"400px" }] }>
                            For more about how we can work together to help families 
                            make memories, leave your details here. 
                            Someone from TimeAway HQ will be with you shortly!

                        </div>
                        <br/>
                        <input 
                            className="form-control" 
                            value={ this.state.name } 
                            placeholder="Name" 
                            onChange={ (e) => this.handleInputChange("name", e ) }/>
                        <input 
                            className="form-control" 
                            value={ this.state.phoneNumber } 
                            placeholder="Phone Number" 
                            onChange={ (e) => this.handleInputChange("phoneNumber", e ) }/>
                        <input 
                            className="form-control" 
                            value={ this.state.email } 
                            placeholder="Email" 
                            onChange={ (e) => this.handleInputChange("email", e ) }/>
                        <textarea 
                            className="form-control" 
                            value={ this.state.message } 
                            placeholder="Message" 
                            style={ styles.textarea }
                            onChange={ (e) => this.handleInputChange("message", e ) }>
                        </textarea>
                        <TaButton onClick={ ( e ) => this.handleSubmit( e ) }>Request a Call</TaButton>
                    </center>
                </div>
            </Grid>
        );
    }
}


module.exports = Radium( AffiliateBody );