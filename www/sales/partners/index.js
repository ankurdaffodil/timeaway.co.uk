import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import ReactDOM from "react-dom";
import { StyleRoot } from "radium";
import FixedNav from "../components/FixedNav";
import ShortBanner from "../components/ShortBanner";
import PartnerBody from "./PartnerBody";
import Footer from "../components/Footer";

function App(){
    return(
        <StyleRoot>
            <FixedNav />
            <div className="navbar-offset">
                <ShortBanner 
                    imgSrc="../img/team.png"
                    heading="Writing Our Story, Together"
                    subHeading="If you are passionate about travel and want to help shape our story, We Want To Hear From You."
                    btnText="About TimeAway"
                    btnHref="/about-us"/>

                <PartnerBody />
                <br/><br/>
                <Footer />
            </div>
        </StyleRoot>
    );
}

document.addEventListener("DOMContentLoaded", function(){
    ReactDOM.render(
        <App />,
        document.getElementById("root")
    );
}); 