import React from "react";
import Radium from "radium";
import _ from "lodash";
import { Grid, Col } from "react-bootstrap";
import TaButton from "../../../common/components/TaButton/";
import typography from "../../constants/typography";

function DestinationCard( props ){
    const cardStyle992 = {
        height: "400px"
    };
    const cardStyle576 = {
        height: "300px"
    };
    const styles = {
        base: { 
            color: "white",
            background : "url(" + props.imageSrc + ") center center",
            marginTop:"5px",
            marginLeft:"5px",
            marginRight:"5px",
            marginBottom:"5px",
            padding: "15px",
            display:"flex",
            flexDirection: "column",
            justifyContent: "flex-end"
        },
        large : {
            height: "500px",
            
            "@media screen and (max-width:992px)" : _.extend( {}, cardStyle992, {

            }),
            "@media screen and (max-width:576px)" : _.extend( {}, cardStyle576, {

            })
        },
        small : {
            height: "245px",
            marginBottom: "10px",
            
            "@media screen and (max-width:992px)" : _.extend( cardStyle992, {
                marginBottom: "5px"
            }),
            "@media screen and (max-width:576px)" : _.extend( cardStyle576, {})
        }
    };

    const stylesArr = [];
    stylesArr.push( styles.base );
    stylesArr.push( styles[ props.cardSize ] );

    return(
        <div style={ stylesArr }>
            { props.children }
        </div>
    );
}

DestinationCard = Radium( DestinationCard );

function DestinationTrio(){
    const styles = {
        container: {
            padding: "10px 5px",
            fontFamily: "montserrat"
        },
        col: {
            padding:"0px"
        },
        heading: _.extend( {}, typography.h2.montserratUppercase, {
            marginTop: "20px",
            fontWeight: "400"
        }),
        cardHeading: _.extend( {}, typography.h2.montserratUppercase, {
            fontWeight: "900",
            textShadow: "0px 3px 6px rgba(0,0,0,1)"
        }),
        cardSubHeading: _.extend( {}, typography.h3.montserrat, {
            marginBottom: "10px",
            textShadow: "0px 3px 6px rgba(0,0,0,1)"
        })
    };
    return(
        <Grid style={ styles.container }>
            <div style={{padding:"5px"}}>
                <div style={styles.heading}>Our Destinations</div>
                <div className="separator"></div>
            </div>
            <Col md={8} style={ styles.col }>
                <DestinationCard cardSize="large" imageSrc="../../img/cornwall.png">
                    <div style={ styles.cardHeading }>CORNWALL</div>
                    <div style={ styles.cardSubHeading }>Harbour Villages &amp; Sandy Beaches</div>
                    <TaButton display="inline-block" margin="2px auto 2px 2px">BEGIN EXPLORING</TaButton>
                </DestinationCard>
            </Col>
            <Col md={4} style={ styles.col }>

                <DestinationCard cardSize="small" imageSrc="../../img/iceland.png">
                    <div style={ styles.cardHeading }>ICELAND</div>
                    <div style={ styles.cardSubHeading }>Harbour Villages &amp; Sandy Beaches</div>
                    <TaButton display="inline-block" margin="2px auto 2px 2px">BEGIN EXPLORING</TaButton>
                </DestinationCard>

                <DestinationCard cardSize="small" imageSrc="../../img/lake-district.png">
                    <div style={ styles.cardHeading }>PEAK DISTRICT</div>
                    <div style={ styles.cardSubHeading }>Harbour Villages &amp; Sandy Beaches</div>
                    <TaButton display="inline-block" margin="2px auto 2px 2px">BEGIN EXPLORING</TaButton>
                </DestinationCard>
            </Col>
        </Grid>
    );
}

module.exports = Radium( DestinationTrio );