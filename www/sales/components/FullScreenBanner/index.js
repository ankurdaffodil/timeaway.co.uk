import React from "react";
import Radium from "radium";
import _ from "lodash";
import TaButton from "../../../common/components/TaButton";
import typography from "../../constants/typography.js";

function FullScreenBanner( props ) {
    const styles = {
        banner: {
            width: "100%",
            height: "100vh",
            backgroundImage: "url(" + props.imgSrc + ")",
            backgroundPosition: "center center",
            backgroundSize: "cover",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            color: "white",
            padding:"3px"
        },
        heading1: _.extend( {}, typography.h1.montserrat, {
            textShadow: "0px 0px 6px black",
            fontWeight:"900",
            letterSpacing:"10px",
            fontSize: "40px",
            "@media screen and (max-width: 530px)" : {
                maxWidth: "300px",
                fontSize: "35px"
            }
        } ),
        heading2: _.extend( {}, typography.h3.montserrat, {
            marginTop: "12px",
            textShadow: "0px 0px 12px black",
            "@media screen and (max-width: 530px)" : {
                maxWidth: "300px"
            }
        })
    };

    return(
        <div style={ styles.banner }>
            <center>
                <div style={ styles.heading1 }>
                    MAKE MEMORIES
                </div>
                <div style={ styles.heading2 }>
                    Discover &amp; book your perfect family holiday.
                </div>

                <br/>
                <TaButton 
                    display="inline-block"
                    inlineToBlock={true}
                    margin="5px"
                    href="/create-holiday">
                    Create My Holiday
                </TaButton>
            </center>
        </div>
    );
}

FullScreenBanner.defaultProps = {

};
module.exports = Radium( FullScreenBanner );