import React from "react";
import Radium from "radium";
import _ from "lodash";
import colors from "../../constants/colors";
import typography from "../../constants/typography";
import TaButton from "../../../common/components/TaButton";

function ShortBanner( props ){
    const styles = {
        banner: {
            height: props.bannerHeight,
            width:"100%",
            backgroundImage: "url(" + props.imgSrc + ")",
            backgroundPosition: "center center",
            backgroundSize: "cover",
            display:"flex",
            alignItems: "center",
            "@media screen and (max-width:767px)" : {
                height: "auto"
            }
        },
        bannerBox: {
            height: "auto",
            maxWidth: "450px",
            width: "100%",
            background: "rgba(0,0,0,0.8)",
            padding: "20px",
            borderLeft: `solid 7px ${colors.brand}`,

            heading: _.extend( {}, typography.h1.playfair, {
                color:"white",
                letterSpacing:"0px"
            }),
            subHeading: _.extend( {}, typography.h4.montserrat, {
                color: "white",
                marginTop:"10px"
            }),
            "@media screen and (max-width:767px)" : {
                maxWidth: "100%",
                height : "auto",
                border : "none",
                padding : "60px 15px"
            }
        },

        container : {
            // default bootstrap container
            // as radium can't style them with media queries
            // consider creating /constants/bootstrapStyles file
            paddingRight: "15px",
            paddingLeft: "15px",
            marginRight: "auto",
            marginLeft: "auto",
            "@media (min-width: 768px)" : {
                width: "750px"
            },
            "@media (min-width: 992px)" : {
                width: "970px"
            },
            "@media (min-width: 1200px)" : {
                width: "1170px"
            },
            // custom
            "@media screen and (max-width:767px)" : {
                padding: "0px", 
                width:"100%", 
                height :"auto"
            }
        }
    };

    return (
        <div style={ styles.banner }>
            <div style={ styles.container }>
                { props.hideBannerBox === false &&
                    <div style={ styles.bannerBox }>
                    
                        <div style={ styles.bannerBox.heading }>
                            { props.heading }
                        </div>
                        
                        {
                            ( props.subheading !== "" ) && 
                                <div style={ styles.bannerBox.subHeading }>
                                    { props.subHeading }
                                </div>
                        }

                        { 
                            ( props.btnHref !== undefined ) &&
                                <TaButton 
                                    display="inline-block" 
                                    margin="15px 0px 0px" 
                                    btnType="default"
                                    href={ props.btnHref }>
                                    { props.btnText }
                                </TaButton>
                        }

                    </div>
                }
            </div>
        </div>
    );
}

ShortBanner.defaultProps = {
    heading: "This is the Heading",
    subHeading: "This is the Subheading",
    btnText: "Button Text",
    hideBannerBox: false,
    bannerHeight: "400px"
};

module.exports = Radium(ShortBanner);