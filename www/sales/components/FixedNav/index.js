import React from "react";
import Radium from "radium";
import _ from "lodash";
import typography from "../../constants/typography";
import { Grid } from "react-bootstrap";
import CSSTransitionGroup from "react-transition-group/CSSTransitionGroup";

const links = [
    ["Create My Holiday", "/create-holiday"],
    ["0333 733 1223", "tel:+4403337331223"],
];

class FixedNav extends React.Component {
    constructor(){
        super();
        this.state = {
            mobNavShowing : false
        };
    }

    toggleMobNavShowing() {
        this.setState( { mobNavShowing : !this.state.mobNavShowing } );
    }

    render(){
        const styles = {
            nav: {
                fontFamily: "montserrat",
                width: "100%",
                height: "60px",
                position: "fixed",
                background: "white",
                zIndex: "1",
                borderBottom: "solid 1px rgba(0,0,0,0.05)"
            },
            navSection: {
                display: "inline-block",
                height: "60px",
                lineHeight: "60px"
            },
            img: {
                height: "100%"
            },
            navRight: {
                float: "right",
                color: "rgba(0,0,0,0.5)",
                "@media screen and (max-width:800px)" : {
                    display:"none"
                }
            },
            navItem: _.extend({}, typography.misc.navHeading, {
                margin: "0px 10px",
                mobile: {
                    display: "block",
                    margin: "0px",
                    width: "100%",
                    height: "60px",
                    lineHeight: "60px"
                }
            } ),

            navItemsMobile: { // container
                position: "fixed",
                width: "100%",
                height:"100vh",
                background:"white",
                left: "0px",
                padding: "10px",
                "@media screen and (min-width:800px)" : {
                    display:"none"
                }
            },
            icons: {
                container: {
                    display: "inline-block"
                },
                icon: {
                    margin: "0px 4px",
                    fontSize:"30px"
                }
            },
            hamburger: {
                width: "60px",
                height: "60px",
                float: "right",
                padding: "20px 17px",
                display: "flex", 
                flexDirection: "column",
                justifyContent: "space-between",
                cursor: "pointer",
                "@media screen and (min-width:800px)" : {
                    display:"none"
                }
            },
            hamburgerLayer: { // sandwiches are composed of layers :D
                width: "100%",
                height: "4px",
                background: "rgba(0,0,0,0.3)",
                borderRadius: "6px"
            }
        };

        return(
            <div style={ styles.nav }>
                <Grid>
                    <div style={ styles.navSection }>
                        <a href="/">
                            <img src="../img/logo.png" style={ styles.img }/>
                        </a>
                    </div>

                    <div style={ [ styles.navSection, styles.navRight ] }>
                        { links.map( ( pair, ix ) => {
                            return <a key={ix} href={ pair[1] }><span style={ styles.navItem }> { pair[0] } </span></a>;
                        } ) }
                    </div>

                    <span>
                        <div style={ styles.hamburger } onClick={ () => this.toggleMobNavShowing() }>
                            <div style={styles.hamburgerLayer}></div>
                            <div style={styles.hamburgerLayer}></div>
                            <div style={styles.hamburgerLayer}></div>
                        </div>

                        <CSSTransitionGroup
                            transitionName="animate-fullscreen"
                            transitionEnterTimeout={300}
                            transitionLeaveTimeout={300}>
                            { this.state.mobNavShowing && 
                                <div style={ [ styles.navItemsMobile ]}>
                                    <Grid>
                                        { links.map( ( pair, ix ) => {
                                            return <a key={ix} href={ pair[1] }><span style={ [styles.navItem, styles.navItem.mobile] }> { pair[0] } </span></a>;
                                        } ) }
                                    </Grid>
                                </div>
                            }
                        </CSSTransitionGroup>
                    </span>
                </Grid>
            </div>
        );
    }
}



module.exports = Radium( FixedNav );