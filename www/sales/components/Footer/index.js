import React from "react";
import _ from "lodash";
import { Grid, Row, Col } from "react-bootstrap";
import Radium from "radium";
import {FaInstagram, FaFacebook, FaPinterest, FaTwitter} from "react-icons/lib/fa";
import typography from "../../constants/typography";
import colors from "../../constants/colors";

const styles = {
    container: {
        borderTop: "solid 1px rgba(0,0,0,0.04)"
    },
    header: _.extend({}, typography.misc.footerHeading, {
        padding: "22px 0px",
        lineHeight: "26px",
        color:"#77B3EC"
    }),
    icons: {
        container: {
            fontSize: "26px",
            padding: "22px 0px"
        },
        icon: {
            margin: "0px 5px"
        }
    },
    ul: {
        listStyle: "none",
        padding: "0px",
        marginTop: "-10px"
    },
    li: _.extend({}, typography.p.montserrat, {
        padding: "8px 0px",
        color: colors.textPreHover,
        ":hover" : colors.textPostHover
    }),
    bottomRow: {
        container: {
            height: "80px",
            marginBottom: "30px"
        },
        img: {
            height: "100%",
            float: "left"
        },
        links: {
            display: "inline-block",
            height: "80px",
            fontSize: "13px",
            color: "rgba(0,0,0,0.3)",
            lineHeight: "80px",
            float: "right",
            "@media screen and (max-width: 992px)" : {
                display:"block",
                width: "100%",
                float:"left"
            }
        }
    }
};

function Footer (){
    return(
        <Grid style={ styles.container }>
            <Row>
                <Col md={3}>
                    <div style={ styles.header }>TIMEAWAY</div>
                    <ul style={ styles.ul }>
                        <li key={1} style={ styles.li }>
                            <a href="/create-holiday">
                                Create My Holiday
                            </a>
                        </li>
                        {/*
                            <li style={ styles.li }>
                                <a href="https://google.com">
                                    Stories
                                </a>
                            </li>
                        */}
                        <li key={2} style={ styles.li }>
                            <a href="/about-us">
                                About Us
                            </a>
                        </li>
                    </ul>
                </Col>
                <Col md={3}>
                    <div style={ styles.header }>LIST WITH US</div>
                    <ul style={ styles.ul }>
                        <li key={3} style={ styles.li }>
                            <a href="/gurus">
                                TimeAway Gurus
                            </a>
                        </li>
                        {/*
                            <li style={ styles.li }>
                                <a href="https://google.com">
                                    TimeAway Guru Stories
                                </a>
                            </li>
                        
                            <li key={4} style={ styles.li }>
                                <a href="/our-standards">
                                    Our Standards
                                </a>
                            </li>
                        */}
                        {/*
                            <li style={ styles.li }>
                                <a href="https://google.com">
                                    The TimeAway Family
                                </a>
                            </li>
                        */}
                        <li key={5} style={ styles.li }>
                            <a href="/photo-guidelines">
                                Photo Guidelines
                            </a>
                        </li>
                    </ul>
                </Col>
                <Col md={3}>
                    <div style={ styles.header }>OUR PARTNERS</div>
                    <ul style={ styles.ul }>
                        <li key={6} style={ styles.li }>
                            <a href="/super-mums">
                                Super Mums
                            </a>
                        </li>
                        <li key={7} style={ styles.li }>
                            <a href="/partners">
                                Partners
                            </a>
                        </li>
                    </ul>
                </Col>
                <Col md={3}>
                    <div style={ styles.icons.container }>
                        <a href="https://instagram.com/familytimeaway">
                            <FaInstagram style={ styles.icons.icon }/>
                        </a>
                        <a href="https://twitter.com/familytimeaway">
                            <FaTwitter style={ styles.icons.icon }/>
                        </a>
                        <a href="https://pinterest.com/familytimeaway">
                            <FaPinterest style={ styles.icons.icon }/>
                        </a>
                        <a href="https://facebook.com/familytimeaway">
                            <FaFacebook style={ styles.icons.icon }/>
                        </a>
                    </div>
                </Col>
            </Row>
            <br/>
            <br/>
            <br/>
            <Row style={ styles.bottomRow.container }>
                <img src="../../img/logo.png" style={ styles.bottomRow.img } />
                <div style={ styles.bottomRow.links }>
                    <a href="/policies">
                        <span style={{padding:"0px 10px"}}>
                            Privacy Policy &amp; Cookies
                        </span>
                    </a>
                    | 
                    <span style={{padding:"0px 10px"}}>
                        &copy; TimeAway, Ltd.
                    </span>
                </div>
            </Row>
        </Grid>
    );
}

module.exports = Radium( Footer );