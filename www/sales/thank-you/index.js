import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import _ from "lodash";
import ReactDOM from "react-dom";
import { StyleRoot } from "radium";
import Radium from "radium";
import { Grid } from "react-bootstrap";
import FixedNav from "../components/FixedNav";
import Footer from "../components/Footer";
import TaButton from "../../common/components/TaButton";
import typography from "../constants/typography";
import { FaFacebookSquare, FaTwitter} from "react-icons/lib/fa";

const ThanksBody = Radium(function() {
    const styles = {
        heading: _.extend({}, typography.h1.playfair, {
            color: "white"
        }),
        subHeading: _.extend({}, typography.h3.montserrat, {
            color: "white",
            maxWidth: "450px",
            margin:"15px 0px"
        })
    };
    return(
        <div style={{ padding:"100px 0px"}}>
            <div style={ styles.heading }>
                Thank You.
            </div> 
            <div style={ styles.subHeading }>
                One of us at TimeAway HQ will be in touch very shortly. We look forward to hearing more about you - from all of us at TimeAway, Thank You.
            </div>
            <TaButton 
                btnType="dark" 
                display="inline-block" 
                href="https://twitter.com/familytimeaway">
                Follow Us <FaTwitter />
            </TaButton>
            <TaButton 
                btnType="transparentWhite" 
                display="inline-block" 
                margin="0px 0px 0px 5px"
                href="https://facebook.com/familytimeaway">
                Find Us <FaFacebookSquare />
            </TaButton>
        </div>
    );
});

function App(){
    return(
        <StyleRoot>
            <FixedNav />
            <div className="navbar-offset grad-blue-skies">
                <Grid>
                    <ThanksBody />
                </Grid>
            </div>
            <Footer />
        </StyleRoot>
    );
}

document.addEventListener("DOMContentLoaded", function(){
    ReactDOM.render(
        <App />,
        document.getElementById("root")
    );
});