import React from "react";
import Radium from "radium";
import axios from "axios";
import { unwrap } from "../lib/util";
import _ from "lodash";
import { Grid, Row, Col } from "react-bootstrap";
import typography from "../constants/typography";
import colors from "../constants/colors";
import TaButton from "../../common/components/TaButton";

class TravelExpertBody extends React.Component {
    constructor(){
        super();
        this.state = {
            name : "",
            phoneNumber : "",
            email : "",
            message: ""
        };
        this.handleInputChange = this.handleInputChange.bind(this);
    }
    handleInputChange( stateKey, e ){
        const stateObj = {};
        stateObj[ stateKey ] = e.target.value;
        this.setState( stateObj, () => console.log( this.state ) );

    }
    handleSubmit( e ) {
        e.preventDefault();
        axios({
            url: "/api/send_email_form",
            method: "POST",
            data: {
                subject: "TimeAway Guru Application",
                name: this.state.name,
                phoneNumber: this.state.email,
                email: this.state.phoneNumber,
                message: this.state.message
            }
        })
        .then( unwrap )
        .then( window.location = "/thank-you" );
    } 
    render(){
        const styles = {
            heading: _.extend({}, typography.h1.playfair, {

            }),
            subHeading: _.extend({}, typography.h2.montserrat, {
                marginBottom: "15px",
                maxWidth: "550px"
            }),
            paragraph: _.extend({}, typography.p.montserrat, {
                color: colors.textDefault
            }),
            trioHeading: _.extend({}, typography.h4.montserratUppercase, {
                marginBottom: "20px",
                color: colors.brand
            }),
            formBox: {
                width: "100%",
                maxWidth: "550px",
                marginLeft: "auto",
                marginRight: "auto",
                height: "auto",
                background: "#EDF5FB",
                padding: "20px",
                boxShadow: "0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)",
                heading: _.extend( {}, typography.h3.montserratUppercase, {
                    color: colors.brand
                }),
                paragraph: _.extend( {}, typography.p.montserrat, {
                    color: colors.textDefault
                }),
                image: {
                    width: "100%",
                    maxWidth: "270px"
                }
            }
        };
        return(
            <Grid>
                <Row>
                    
                    <Col xs={12}>
                        <div style={ [styles.heading, {marginTop: "50px"}] }>TimeAway Gurus</div> 
                        <div className="separator"></div>
                        <div style={ styles.subHeading }>TimeAway Guru&prime;s are Experts in Family Travel. They List Experiences With Us that Make Memories.</div> 
                    </Col>

                    <Col md={12}>
                        <div style={ styles.paragraph }>
                            Listing on TimeAway is completely free. Share your best holiday offerings with our families. 
                            Let them know where they’ll stay and what activities you have to offer.
                            Families can get in touch with you directly through our online messaging service. 
                            You can review and confirm all bookings, and give our families reassurance of a great 
                            experience. Benefit from two-way calendar sync with all iCal calendars.
                            TimeAway’s secure payment platform allows us to payout to you wherever you are in the world. 
                            Choose from a number of payout options to suit your business needs.
                            <br/><br/><br/>

                            <Row>
                                <Col md={4}>
                                    <div style={ styles.trioHeading }>
                                        Work Together
                                    </div>
                                    <div style={ styles.paragraph }>
                                        Families all over the UK can book their perfect travel 
                                        experience through TimeAway. We look for small, independent 
                                        travel experts who create unique experiences for our families. 
                                        We only charge 10% commission on each booking
                                    </div>
                                </Col>
                                <Col md={4}>
                                    <div style={ styles.trioHeading }>
                                        Take Back Control
                                    </div>
                                    <div style={ styles.paragraph }>
                                        Our experts have total control over the price and content of their listings. 
                                        They can message families directly through our online messaging service. 
                                        We have big plans and a new fairer way of doing business. 
                                        So what are you waiting for? Get involved.
                                    </div>
                                </Col>
                                <Col md={4}>
                                    <div style={ styles.trioHeading }>
                                        Join The Mission
                                    </div>
                                    <div style={ styles.paragraph }>
                                        We want to make the world easier and more fun for families to discover and explore together. 
                                        Travel raises the aspirations of our children and we believe every family should be able to 
                                        find the perfect experience for their valuable family TimeAway
                                        <br/><br/>
                                    </div>
                                </Col>
                            </Row>
                            <br/><br/><br/>

                            <div style={ styles.formBox }>
                                <center>
                                    <img src="../img/guru-logo.png" style={ styles.formBox.image } />
                                    <div className="separator"></div>
                                    <div style={ styles.formBox.heading }>Find Out More</div> 
                                    <br/>
                                    <div style={ [styles.formBox.paragraph, {maxWidth: "300px"}] }>
                                        If you would like to find out more about the opportunities there are to work with us,
                                        leave your details here. We&prime;ll be in touch very soon.
                                    </div>
                                    <br/>
                                    <input 
                                        className="form-control" 
                                        value={ this.state.name } 
                                        placeholder="Name" 
                                        onChange={ (e) => this.handleInputChange("name", e ) }/>
                                    <input 
                                        className="form-control" 
                                        value={ this.state.phoneNumber } 
                                        placeholder="Phone Number" 
                                        onChange={ (e) => this.handleInputChange("phoneNumber", e ) }/>
                                    <input 
                                        className="form-control" 
                                        value={ this.state.email } 
                                        placeholder="Email" 
                                        onChange={ (e) => this.handleInputChange("email", e ) }/>
                                    <textarea 
                                        className="form-control" 
                                        value={ this.state.message } 
                                        placeholder="Tell us a little about your Travel Business" 
                                        onChange={ (e) => this.handleInputChange("message", e ) }
                                        style={{resize: "none", height: "100px"}}>
                                    </textarea>

                                    <TaButton onClick={ ( e ) => this.handleSubmit( e ) }>Request a Call</TaButton>
                                </center>
                            </div>
                            
                            <br/>
                            <br/>
                            <br/>
                        </div>
                    </Col>

                </Row>
            </Grid>
        );
    }
}

module.exports = Radium( TravelExpertBody );