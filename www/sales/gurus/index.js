import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import ReactDOM from "react-dom";
import { StyleRoot } from "radium";
import FixedNav from "../components/FixedNav";
import ShortBanner from "../components/ShortBanner";
import GuruBody from "./GuruBody";
import Footer from "../components/Footer";

function App(){
    return(
        <StyleRoot>
            <FixedNav />
            <div className="navbar-offset">
                <ShortBanner 
                    imgSrc="../img/travel-guru.png"
                    heading="Work With Us."
                    subHeading="Join Our Community of Family-Travel Experts, join the TimeAway family today."
                    btnText="About TimeAway"
                    btnHref="/about-us"/>
                <GuruBody />
                <Footer />
            </div>
        </StyleRoot>
    );
}

document.addEventListener("DOMContentLoaded", function(){
    ReactDOM.render(
        <App />,
        document.getElementById("root")
    );
});