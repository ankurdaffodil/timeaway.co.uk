import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import ReactDOM from "react-dom";
import { StyleRoot } from "radium";

import FixedNav from "./components/FixedNav";
import FullScreenBanner from "./components/FullScreenBanner";
import Footer from "./components/Footer";

function App(){
    return(
        <StyleRoot>
            <FixedNav />
            
            <FullScreenBanner 
                imgSrc={"./img/index.png"}
                fontFamily="montserrat" />
            <Footer />
        </StyleRoot>
    );
}

document.addEventListener("DOMContentLoaded", function(){
    ReactDOM.render(
        <App />,
        document.getElementById("root")
    );
});