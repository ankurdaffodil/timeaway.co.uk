import React from "react";
import _ from "lodash";
import Radium from "radium";
import axios from "axios";
import { Grid, Row, Col } from "react-bootstrap";
import typography from "../constants/typography";
import colors from "../constants/colors";
import TaButton from "../../common/components/TaButton";
import Pricing from "./Pricing";
import { unwrap } from "../lib/util";

class SuperMumBody extends React.Component {
    constructor() {
        super();
        this.state = {
            name: "",
            email: "",
            phoneNumber: ""
        };
        this.handleInputChange = this.handleInputChange.bind(this);
    }
    handleInputChange( stateKey, e ){
        const stateObj = {};
        stateObj[ stateKey ] = e.target.value;
        this.setState( stateObj );
    }
    handleSubmit( e ) {
        e.preventDefault();
        axios({
            url: "/api/send_email_form",
            method: "POST",
            data: {
                subject: "Super Mum Application",
                name: this.state.name,
                phoneNumber: this.state.email,
                email: this.state.phoneNumber
            }
        })
        .then( unwrap )
        .then( window.location = "/thank-you" );

    } 
    render(){
        const styles = {
            heading: _.extend({}, typography.h1.playfair, {
                marginTop:"30px"
            }),
            subHeading: _.extend({}, typography.h3.montserrat, {
                marginTop: "10px",
                maxWidth: "400px"
            }),
            sectionHeading: _.extend({}, typography.h3.montserratUppercase, {
                marginTop: "50px",
                marginLeft: "0px",
                marginRight: "0px",
                marginBottom: "10px",
                color: colors.brandLight
            }),
            paragraph: _.extend({}, typography.p.montserrat, {
                color: colors.textDefault
            }),
            pricingRow: {
                padding: "40px 40px 60px",
                height: "auto",
                overflow: "auto"
            },
            formBox: {
                maxWidth: "550px",
                marginLeft: "auto",
                marginRight: "auto",
                marginTop:"50px",
                width: "100%",
                height: "auto",
                background: "#EDF5FB",
                padding: "20px",
                boxShadow: "0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)",
                heading: _.extend( {}, typography.h3.montserratUppercase, {
                    color: colors.brand
                }),
                paragraph: _.extend( {}, typography.p.montserrat, {
                    color: colors.textDefault
                }),
                image: {
                    width: "100%",
                    maxWidth: "270px"
                }
            }
        };
        return(
            <div>
                <Grid>
                    <Row>
                        <Col md={12}>
                            <div style={ styles.heading }>The Super Mum Program</div>
                            <div style={ styles.subHeading }>Work Your Own Hours, Doing Something You Love, For People You Love.</div>
                            <div className="separator"></div>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={12}>
                            <div style={ styles.paragraph }>
                                If you&prime;re one of those Mums who loves researching where to take your family on holiday, or if you&prime;re the &Prime;go-to&Prime; Mum for other parents to gain inspiration for their next trip, we&prime;ve been waiting for you! 
                                <b> Join the TimeAway family as a Super Mum and earn income for doing something you&prime;ll love: finding amazing family holidays for your friends</b>.
                                <br/><br/>
                                As a Super Mum, you’ll be your own boss, work hours that suit you, and we’ll make sure you have the first opportunities to try our exclusive new holidays. Being a Super Mum is all about sharing your favourite holidays - 
                                whether it’s by spreading the word mouth-to-mouth, posting on social media, or emailing your holiday ideas out to your network. Every time your personal code is used at the time of booking on our website, you’ll earn up to £150.
                                Whether a weekend trip to France, or a Safari in Sri Lanka, we’ll work together to curate incredible family trips.
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={4}>
                            <div style={ styles.sectionHeading }>Joining Our Mission</div>
                            <div style={ styles.paragraph}>
                                TimeAway’s mission is to make the world easier and more enjoyable for families to discover and explore. Travel raises the aspirations of our children and we believe every family should be able to find the perfect experience for their quality family TimeAway.
                            </div> 
                        </Col>
                        <Col md={4}>
                            <div style={ styles.sectionHeading }>What&prime;s Coming In 2017?</div>
                            <div style={ styles.paragraph }>
                                The TimeAway&prime;s team is always coming up with creative ways to help you do this, in seamless and fun ways. Our Super Mum platform is due to be released later this year. Here you will find tools for elegant and fun ways to share holidays with your friends.
                            </div>
                        </Col>
                        <Col md={4}>
                            <div style={ styles.sectionHeading }>Requirements</div>
                            <div style={ styles.paragraph }>
                                We’re not fussed about whether you have experience in sales or marketing. Our customers are looking for a friendly, helpful and reliable advice from like-minded parents. Our dedicated team will be on-hand to support you every step of the way. See how this might fit around your life.
                            </div>
                        </Col>
                    </Row>
                    <br/><br/>
                </Grid>
                
                <Row>
                    <div className="grad-blue-skies" style={ styles.pricingRow }>
                        <center style={[styles.heading, {color: "white"}]}>Be Your Own Boss.</center>
                        <br/><br/>
                        <Grid>
                            <Col xs={12}>
                                <Pricing /> 
                            </Col>
                        </Grid>
                    </div>
                </Row>

                <Grid>
                    <div style={ styles.formBox }>
                        <center>
                            <img src="../img/super-mum-logo.png" style={ styles.formBox.image } />
                            <div className="separator"></div>
                            <div style={ styles.formBox.heading }>Find Out More</div> 
                            <br/>
                            <div style={ [styles.formBox.paragraph, { maxWidth: "350px"}] }>
                                If you would like to find out more about our Super Mums program, please leave your details here, then Faye or Wahab will be in touch with you very shortly.
                            </div>
                            <br/>
                            <input 
                                className="form-control" 
                                value={ this.state.name } 
                                placeholder="Name" 
                                onChange={ (e) => this.handleInputChange("name", e ) }/>
                            <input 
                                className="form-control" 
                                value={ this.state.phoneNumber } 
                                placeholder="Phone Number" 
                                onChange={ (e) => this.handleInputChange("phoneNumber", e ) }/>
                            <input 
                                className="form-control" 
                                value={ this.state.email } 
                                placeholder="Email" 
                                onChange={ (e) => this.handleInputChange("email", e ) }/>
                            <TaButton onClick={ ( e ) => this.handleSubmit( e ) }>Request a Call</TaButton>
                        </center>
                    </div>
                    <br/><br/>
                </Grid>
            </div>
        );
    }
    
}

module.exports = Radium( SuperMumBody );