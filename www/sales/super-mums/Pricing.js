import React from "react";
import _ from "lodash";
import typography from "../constants/typography";
import Radium from "radium";

function Pricing (){
    const styles = {
        pricingCol: {
            width: "25%",
            minHeight: "260px",
            background: "white",
            float:"left",
            "@media screen and (max-width:600px)" : {
                width: "100%",
                marginBottom: "20px"
            }
        },
        pricingSmallText: {
            fontSize: "10px",
            letterSpacing: "2px",
            textTransform: "uppercase"
        },
        price: {
            background: "#5a5a5a",
            minHeight: "50px",
            padding: "30px 14px",
            textAlign: "center",
            fontSize: "30px",
            color: "white"
        },
        pricingBlurb: _.extend({}, typography.p, {
            padding : "20px",
            fontSize: "12px",
            textAlign: "center"
        } ) 
    };
    return(
        <center>
            <div className="md-card" style={ styles.pricingCol }>
                <div style={ [styles.price, {background: "#216FBB"}] }>
                    <div style={ styles.pricingSmallText }>Receive</div>
                    &pound; 150
                    <div style={ styles.pricingSmallText }>Per Holiday</div>
                </div>
                <div style={ styles.pricingBlurb }>
                    When you refer a holiday worth over &pound;2500 (luxury, safaris, tours)
                </div>
            </div>
            <div className="md-card" style={ styles.pricingCol }>
                <div style={ [styles.price, {background: "#257cd1"}] }>
                    <div style={ styles.pricingSmallText }>Receive</div>
                    &pound; 100
                    <div style={ styles.pricingSmallText }>Per Holiday</div>
                </div>
                <div style={ styles.pricingBlurb }>
                    When you refer a holiday worth over &pound;1500 (summer holidays, long breaks)
                </div>
            </div>
            <div className="md-card" style={ styles.pricingCol }>
                <div style={ [styles.price, {background: "#3489db"}] }>
                    <div style={ styles.pricingSmallText }>Receive</div>
                    &pound; 50
                    <div style={ styles.pricingSmallText }>Per Holiday</div>
                </div>
                <div style={ styles.pricingBlurb }>
                    When you refer a holiday worth over &pound;1000 (weekend trips, short breaks)
                </div>
            </div>
            <div className="md-card" style={ styles.pricingCol }>
                <div style={ [styles.price, {background: "#4a95df"}] }>
                    <div style={ styles.pricingSmallText }>Receive</div>
                    &pound; 20
                    <div style={ styles.pricingSmallText }>Per Holiday</div>
                </div>
                <div style={ styles.pricingBlurb }>
                    When you refer a holiday worth over &pound;500 (weekend trips)
                </div>
            </div>
        </center>
    );
}

module.exports = Radium( Pricing );