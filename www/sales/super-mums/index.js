import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import ReactDOM from "react-dom";
import { StyleRoot } from "radium";

import ShortBanner from "../components/ShortBanner";
import FixedNav from "../components/FixedNav";
import SuperMumBody from "./SuperMumBody";
import Footer from "../components/Footer";

function App(){
    return (
        <StyleRoot>
            <FixedNav />
            <div className="navbar-offset">
                <ShortBanner 
                    imgSrc="../img/super-mum.png"
                    heading="Definition:"
                    subHeading="A TimeAway Super Mum is a “go-to” Mum for other parents to find inspiration for their next trip."
                    btnText="About TimeAway"
                    btnHref="/about-us"/>

                <SuperMumBody />
                
                <Footer />
            </div>
        </StyleRoot>
    );
}

document.addEventListener("DOMContentLoaded", function(){
    ReactDOM.render(
        <App />,
        document.getElementById("root")
    );
});