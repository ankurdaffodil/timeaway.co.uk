module.exports = {
    h1: {
        montserratUppercase: { 
            fontFamily: "'montserrat', sans-serif", 
            fontSize: "30px", 
            letterSpacing: "5px", 
            fontWeight: "900",
            textTransform: "uppercase"
        },
        montserrat: { 
            fontFamily: "'montserrat', sans-serif", 
            fontSize: "30px",
            fontWeight: "300"
        },
        playfair: {
            fontFamily: "'Playfair Display', serif",
            fontSize: "30px",
            fontWeight: "700"
        }
    },
    h2: {
        montserrat: { 
            fontFamily: "'montserrat', serif",
            fontWeight: "300",
            letterSpacing: "1px",
            fontSize: "24px"
        },
        montserratUppercase: { 
            fontFamily: "'montserrat', serif", 
            fontSize: "24px", 
            letterSpacing: "5px", 
            fontWeight: "300",
            textTransform: "uppercase"
        },
        playfair: {
            fontFamily: "'Playfair Display', serif",
            fontSize: "24px",
            fontWeight: "700"
        }
    },
    h3: {
        montserrat: {
            fontFamily: "'montserrat', serif",
            fontSize: "18px",
            fontWeight: "300",
            letterSpacing: "0.5px"
        },
        montserratUppercase: {
            fontFamily: "'montserrat', serif",
            fontSize: "18px",
            fontWeight: "300",
            textTransform: "uppercase",
            letterSpacing: "3px"
        },
        playfair: {
            fontFamily: "'Playfair Display', serif",
            fontSize: "18px",
            fontWeight: "700"
        }
    },
    h4: {
        montserrat: {
            fontFamily: "'montserrat', serif",
            fontSize: "15px",
            fontWeight: "300",
            letterSpacing: "0.5px"
        },
        montserratUppercase: {
            fontFamily: "'montserrat', serif",
            fontSize: "15px",
            fontWeight: "300",
            textTransform: "uppercase",
            letterSpacing: "3px"
        },
    },
    misc: {
        footerHeading: {
            fontSize: "15px",
            letterSpacing: "3px"
        },
        navHeading: {
            letterSpacing: "3px",
            fontSize: "12px",
            textTransform: "uppercase",
        },
        capsLabel: {
            fontFamily: "'montserrat', serif",
            fontSize: "10px",
            fontWeight: "300",
            textTransform: "uppercase",
            letterSpacing: "3px"
        }
    },
    p: {
        montserrat: {
            fontFamily: "'montserrat', serif",
            fontSize: "15px",
            fontWeight: "400"
        }
    }
};