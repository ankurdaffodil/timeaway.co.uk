module.exports = {
    brand : "#3498DB",
    brandDark : "#0A66A3",
    brandLight : "#6588A9",
    brandMediumLight : "#b5c9d9",
    brandSuperLight: "#cae0f2",
    
    textDefault : "rgba(0,0,0,0.65)",
    textLight : "rgba(0,0,0,0.4)",
    textPreHover : "rgba(0,0,0,0.5)",
    textPostHover : "rgba(0,0,0,0.85)"
};