import React from "react";
import Radium from "radium";
import _ from "lodash";
import { Grid, Row, Col} from "react-bootstrap";
import typography from "../constants/typography";
import colors from "../constants/colors";

function ValuesBody(){
    const styles = {
        container: {
            paddingTop: "10px",
            paddingBottom: "10px"
        },
        heading: _.extend({}, typography.h1.playfair, {
            marginTop: "30px",
            color: colors.textDefault
        })
    };
    return(
        <Grid style={ styles.container }>
            <Row>
                <Col xs={12}>
                    <div style={ styles.heading }>About Us.</div>
                    <div className="separator"></div>
                </Col>    
                <Col md={12}>

                    Our mission is simple: We want to make the world easier and more enjoyable for 
                    families to discover and explore. We all have cherished memories from our childhood holidays. 
                    Each day is infused with family harmony, and the laughter, stories, and pleasure that you gain 
                    makes them a powerful source of nostalgia. We know that you want to provide even better experiences 
                    for your children, and as times have changed, it’s now possible to take your family to every corner 
                    of the earth. Just tell us what you want, and we’ll make it happen.
                    <br/><br/>
                    We passionately believe there is so much to gain from taking 
                    your children to interesting and unfamiliar places. From trying new foods 
                    to encountering different cultures, the learning opportunities that a 
                    child gains from travelling are endless. Learning doesn’t stop when 
                    the school bell rings. Travel is education. That’s why we work with 
                    the best suppliers around the world to bring to adventurous, 
                    authentic and experiential holidays, all designed especially for families.
                    <br/><br/>
                    Don’t settle for boring, go exploring. Make memories with TimeAway.
                    <br/><br/>
                </Col>
            </Row>
        </Grid>
    );
}

module.exports = Radium( ValuesBody );