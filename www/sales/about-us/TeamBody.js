import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import Radium from "radium";
import _ from "lodash";
import { Grid, Row, Col } from "react-bootstrap";
import typography from "../constants/typography";
import colors from "../constants/colors";

function TeamBody(){
    const styles = {
        heading: _.extend( {}, typography.h1.playfair, {
            marginTop: "40px",
            ƒcolor: colors.textDefault
        }),
        subHeading: _.extend( {}, typography.h2.montserrat, {
            marginBottom: "15px",
            color: colors.textDefault,
            maxWidth: "500px"
        }),
        teamMemberImg: {
            width: "100%",
            marginBottom: "17px"
        },
        teamMemberHeading: _.extend( {}, typography.h3.playfair, {
            color: colors.brandLight,
            "@media screen and (max-width:767px)" : typography.h2.playfair
        }),
        teamMemberBio: _.extend( {}, typography.p.montserrat, {
            color: colors.textDefault,
            marginBottom: "30px"
        })
    };
    return( 
        <Grid>
            <div style={ styles.heading }>
                Meet The Team.
            </div>
            <div className="separator"></div>
            <div style={ styles.subHeading }>
               Our Mission: To Bring the Best of The World, To Families. We Help Families <b>Make Memories</b>.
            </div>

            <Row>
                <Col md={6}>
                    <img src="./img/wahab.png" style={ styles.teamMemberImg }/>
                    <div style={ styles.teamMemberHeading }>Wahab Ahmad</div>
                    <div className="separator light-grey"></div>
                    <div style={ styles.teamMemberBio }>
                        <b>Favourite Destination:</b> Paris <br/>
                        <b>Favourite Family Holiday:</b> A week on a fishing island in the Phillipines<br/>
                        <b>Bucket List:</b> To see the northern lights<br/>
                    </div>
                </Col>
                <Col md={6}>
                    <img src="./img/faye.png" style={ styles.teamMemberImg }/>
                    <div style={ styles.teamMemberHeading }>Faye Woodcock</div>
                    <div className="separator light-grey"></div>
                    <div style={ styles.teamMemberBio }>asda sdsad asd kasd klsa dkjsadajs dlkajsd kljasd lkaj sdklas jdklasj klasjd lkajsdkladklasdklajsd klajdslkjads kljda kljaklsjklasj kldas</div>
                </Col>
            </Row>
        </Grid>
    );
}

module.exports = Radium( TeamBody );