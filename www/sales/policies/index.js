import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import ReactDOM from "react-dom";
import _ from "lodash";
import { StyleRoot } from "radium";

import typography from "../constants/typography";
import colors from "../constants/colors";
import FixedNav from "../components/FixedNav";
import Footer from "../components/Footer";
import { Grid } from "react-bootstrap";

function Policies(){
    const styles = {
        heading: _.extend( {}, typography.h1.playfair, { 
            color: colors.textDefault,
            marginTop: "30px"
        }),
        subHeading: _.extend( {}, typography.h3.montserrat, { 
            maxWidth:"400px", 
            marginTop:"10px", 
            "color" : "rgba(0,0,0,0.5)" 
        } ),
        subHeadingUppercase: _.extend( {}, typography.h3.montserratUppercase, { marginTop: "50px", color:"#6588A9" } ),
        paragraph: _.extend( {}, typography.p.montserrat, { marginTop:"14px", color:"rgba(0,0,0,0.5)"})
    };
    return(
        <StyleRoot>
            <FixedNav />

            <Grid className="navbar-offset">
                <div style={ styles.heading }>Our Privacy & Cookies Policies</div>
                <div style={ styles.subHeading }>At TimeAway, we are committed to treating your personal information responsibly and ensuring that your privacy is protected.</div>
                <div className="separator"></div>


                <div style={ [styles.subHeadingUppercase, {marginTop: "0px"}] }>
                    Privacy Policy
                </div>
                <div style={ styles.paragraph}>
                    This privacy policy sets out how TimeAway uses and protects any information that you give to us when you use this website. Should we ask you to provide certain information by which you can be identified when using this website, then you can be assured that it will only be used in accordance with this privacy statement. We may change this policy from time to time by updating this page. You should check this page from time to time to ensure that you are happy with any changes.
                </div>


                <div style={ styles.subHeadingUppercase }>
                    What We Collect
                </div>
                <div style={ styles.paragraph}>
                    We may collect the following information:
                    <br/><br/>
                    <ul>
                        <li>Your name, address and company details</li>
                        <li>Contact information such as email address, or telephone number.</li>
                        <li>Information relating to your preferences and interests.</li>
                        <li>Other information, at your discretion, relevant to customer surveys and/or offers.</li>
                    </ul>
                </div>


                <div style={ styles.subHeadingUppercase }>
                    How We Use Information
                </div>
                <div style={ styles.paragraph}>
                    We require this information to understand your needs and provide you with a better service, and in particular for the following reasons:
                    <br/><br/>
                    <ul>
                        <li>Our own internal record keeping.</li>
                        <li>We may use your information for the purposes of providing you with our services and customer care.</li>
                        <li>We may need to disclose your information to public authorities, if requested by them or required by law.</li>
                        <li>We may use the information to improve our products and services.</li>
                        <li>We may periodically send promotional emails about new products, special offers or other information which we think you may find interesting using the email address which you have provided, if you have agreed to receive this information.</li>
                        <li>We are committed to ensuring that your information is secure. In order to prevent unauthorised access or disclosure we have put in place suitable physical, electronic and managerial procedures to safeguard and secure the information we collect online.</li>
                    </ul>
                </div>

                
                <div style={ styles.subHeadingUppercase }>
                    Links to Other Websites
                </div>
                <div style={ styles.paragraph}>
                    Our website may contain links to other websites of interest. However, once you have used these links to leave our site, you should note that we do not have any control over that other website. Therefore, we cannot be responsible for the protection and privacy of any information which you provide whilst visiting such sites and such sites are not governed by this privacy statement. You should exercise caution and look at the privacy statement applicable to the website in question.Controlling your personal information
                    <br/><br/>
                    We will not sell, distribute or lease your personal information to third parties unless we have your permission or are required by law to do so. We may transfer your information to the purchaser of the assets or shares of our company.
                    <br/><br/>
                    If you believe that any information we are holding on you is incorrect or incomplete, please write or email us as soon as possible. We will promptly correct any information found to be incorrect.
                    <br/><br/>
                </div>


                <div style={ styles.subHeadingUppercase }>
                    Contact Us
                </div>
                <div style={ styles.paragraph }>You may choose to restrict the collection or use of your personal information by emailing us at info@timeaway.co.uk.</div>
            </Grid>
            <Footer /> 
        </StyleRoot>
    );
}

document.addEventListener("DOMContentLoaded", function(){
    ReactDOM.render(
        <Policies />,
        document.getElementById("root")
    );
});