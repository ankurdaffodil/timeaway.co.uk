import React from "react";
import Radium from "radium";
import colors from "../../../sales/constants/colors";

function TaButton( props ){
    // Create all style classes
    let styles = {
        base: {
            fontFamily: "montserrat",
            textTransform: props.keepCase == true ? "none" : "uppercase",
            letterSpacing: props.keepCase == true ? "1px" : "5px",
            padding: props.padding ? props.padding : "7px 20px",
            borderRadius: "5px",
            fontSize: "12px",
            outline:"none",
            margin: props.margin,
            textShadow: "0px 2px 3px rgba(0,0,0,0.8)"
        },
        // display
        inline : {
            display: "inline-block"
        },
        inlineToBlock : {
            "@media screen and (max-width: 530px)" : {
                display:"block",
                width:"90%"
            }
        },
        block: {
            display: "block",
            width: "100%"
        },
        // colors
        dark: {
            background: colors.brandDark,
            color: "white",
            border: "solid 1px rgba(255,255,255,0.1)"
        },
        default: {
            backgroundColor: colors.brand,
            color: "white",
            border: "solid 1px rgba(255,255,255,0.1)"
        },
        light: {
            background: colors.brandLight,
            color: "white",
            border: "solid 1px rgba(255,255,255,0.1)"
        },
        superLight: {
            background: colors.brandSuperLight,
            color: "white",
            border: "solid 1px rgba(255,255,255,0.1)"
        },
        transparentWhite: {
            background: "none",
            color: "rgba(255,255,255,0.9)",
            border: "solid 2px rgba(255,255,255,0.4)"
        }
    };
        
    // Pick n Mix style classes
    let stylesArr = [];
    stylesArr.push( styles.base );
    stylesArr.push( props.display == "inline-block" ? styles.inline : styles.block );
    stylesArr.push( props.inlineToBlock == true ? styles.inlineToBlock : {} );
    stylesArr.push( styles[ props.btnType ] );


    // conditionally add anchor attributes
    let anchorProps = {};
    if( props.href !== "" ) anchorProps.href = props.href; 
    if( props.onClick !== undefined ) anchorProps.onClick = props.onClick;

    return(
        <a { ...anchorProps }>
            <button style={ stylesArr }>
                { props.children }
            </button>
        </a>
    );
}

TaButton.defaultProps = {
    display: "block",
    btnType: "default",
    margin: "5px 0px",
    inlineToBlock: false,
    href: ""
};

module.exports = Radium( TaButton );